import 'package:flutter/material.dart';
import 'package:slur_machine/theme/colors.dart';
import 'package:slur_machine/slurs.dart' as slurs;
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Slur Machine',
      theme: _slurMachineTheme,
      home: MyHomePage(title: 'Slur Machine'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final searchTextController = TextEditingController();

  @override
  void dispose() {
    searchTextController.dispose();
    super.dispose();
  }

  Future<String> _wordFetch(String word) async {
    print('Word Fetch initiated on: $word');
    var fetchedSlurs = await slurs.fetchWord(word, _fetchDate());
    return fetchedSlurs;
  }

  void searchInputText(String word) {
    _wordFetch(word).then((result) => {
      result == "error" ? _alertError() : _copyWordAndNotifiyUser(result)
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            new Row (
              children: <Widget>[
                new Flexible(
                  child: TextFormField(
                    controller: searchTextController,   
                    decoration: InputDecoration(
                      hintText: 'What\'s the dirty word?',
                    ),
                    style: TextStyle(color: Colors.black, fontSize: 18.0),
                    onEditingComplete: () =>  searchInputText(searchTextController.text),
                  ),
                ) ,
                IconButton(
                  icon: Icon(Icons.search),
                  tooltip: 'search',
                  onPressed: () {
                    searchInputText(searchTextController.text);
                  },
                )
              ],
            ),
            Text(
              'Quick List',
            ),
            new Expanded (
              child: ListView.builder(
              itemCount: commonSlurs.length,
              itemBuilder: (BuildContext ctx, int index) {
                return FutureBuilder(
                  future: _wordFetch(commonSlurs[index]),
                  builder: (context, snapshot) {
                    switch(snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                      case ConnectionState.active:
                        return Align (
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        );
                      case ConnectionState.done:
                        if(snapshot.hasError) {
                          return Text('Could not load basic list');
                        } else {
                          var pageData = snapshot.data;
                          return _buildSlurButton(commonSlurs[index],pageData.toString());
                        } 
                    }
                  } ,
                );
              },
            )
            )
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _copyWordAndNotifiyUser(result) {
    print('search word found copying to clipboard');
    _copyDataToClipBoard(result);
    _sendUserCopyToast(result);
  }

  void _alertError() {
    print('running alert');
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: new Text('Error'),
          content: new Text('Sorry, no translation found'),
          contentTextStyle: TextStyle(color: Colors.black, fontSize: 18.0),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  } 
}

ButtonBar _buildSlurButton(String original, String translated) {
  return new ButtonBar(
    children: <Widget>[
      RaisedButton(
        child: Text('$original | $translated'),
        elevation: 8.0,
        color: secondaryColor,

        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(7.0))
        ),
        onPressed: () {
          _copyDataToClipBoard(translated);
          _sendUserCopyToast(translated);
        },
      )
    ],
  );
}

void _copyDataToClipBoard(String text) {
  Clipboard.setData(ClipboardData(text: text));
}

void _sendUserCopyToast(String text) {
  Fluttertoast.showToast(
        msg: '$text copied to clipboard',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0
    );
}

String _fetchDate() {
  var currentDate = DateTime.now();
  return _monthTranslator(currentDate.month) + " " + currentDate.day.toString() + " " + currentDate.year.toString();
}

String _monthTranslator(int monthNumber) {
  switch(monthNumber) {
    case 1:
      return "January";
    case 2:
      return "February";
    case 3:
      return "March";
    case 4:
      return "April";  
    case 5:
      return "May";
    case 6:
      return "June";
    case 7:
      return "July";
    case 8:
      return "August";
    case 9:
      return "September";
    case 10:
      return "October";          
    case 11:
      return "November";
    case 12:
      return "December";  
  }
  return "";
}

final List<String> commonSlurs = slurs.commonSlurs;
final ThemeData _slurMachineTheme = _buildSlursTheme();

ThemeData _buildSlursTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    accentColor: secondaryColor,
    primaryColor: primaryColor,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: secondaryColor,
      textTheme: ButtonTextTheme.primary
    ),
    
    textTheme: TextTheme(
      title: TextStyle(color: Colors.black, fontSize: 36.0),
      body1: TextStyle(color: Colors.black, fontSize: 36.0),
      body2: TextStyle(color: Colors.black, fontSize: 36.0),
    ),
    scaffoldBackgroundColor: Colors.white,
    cardColor: backgroundColor,
    textSelectionColor: textColor,
    errorColor: errorColor,
    );
}