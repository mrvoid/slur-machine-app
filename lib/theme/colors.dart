import 'package:flutter/material.dart';

final primaryColor = Colors.blueGrey[700];
const primaryLight = const Color(0X718792);
const primaryDark = const Color(0X1C313A);
const textColor = Colors.black;

final secondaryColor = Colors.green[300];
const secondaryLight = const Color(0X60ad5e);
const secondaryDark = const Color(0X005005);

const backgroundColor = const Color(0XFFFFFF);
const errorColor = const Color(0XC5032B);

