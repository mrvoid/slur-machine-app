import 'package:http/http.dart' as http;
import 'dart:convert';

final _url = 'https://slur-machine.herokuapp.com/encode/';

Future<String> fetchWord(String word, String date) async {
  var searchUrl = _composeUrl(word, date);
  var response = await http.get(searchUrl);
  Map<String, dynamic> json = jsonDecode(response.body);
  return json.containsKey("word") ? json['word'] : 'error';
}

String _composeUrl(String word, String date) {
  var newUrl = _url + word + '?' + "date=\"$date\"";
  return newUrl;
}

List<String> commonSlurs = ['faggot', 'fag', 'nigger', 'nigga', 'cunt', 'kike', 'retard', 'dipshit', 'tranny', 'degenerate', 'shithead', 'asshole', 'towelhead', 'bitch', 'motherfucker'];